// alert("Hello")

// FETCH
// Syntax:
// fetch(url, options)
// options: GET(default), POST, PUT, DELETE


// Get post data using fetch()
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((data) => showPosts(data))

// Add post data
document.querySelector('#form-add-post').addEventListener('submit', e => {
    e.preventDefault()

    fetch('https://jsonplaceholder.typicode.com/posts', {
        method: 'POST',
        body: JSON.stringify({
            title: document.querySelector('#txt-title').value,
            body: document.querySelector('#txt-body').value,
            userId: 1
        }),
        headers: { 'Content-type': 'application/json; charset=UTF-8' }
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert('Successfully added.')

        document.querySelector('#txt-title').value = null
        document.querySelector('#txt-body').value = null
    })
})

// Show posts
const showPosts = (posts) => {
    let postEntries = ''

    posts.forEach((posts) => {
        postEntries += `
            <div id="post-${posts.id}">
                <h3 id="post-title-${posts.id}">${posts.title}</h3>
                <p id="post-body-${posts.id}">${posts.body}</p>
                <button onclick="editPost('${posts.id}')">Edit</button>
                <button onclick="deletePost('${posts.id}')">Delete</button>
            </div>
        `
    })

    document.querySelector('#div-post-entries').innerHTML = postEntries
}

// Edit Post
const editPost = (id) => {
    let title = document.querySelector(`#post-title-${id}`).innerHTML
    let body = document.querySelector(`#post-body-${id}`).innerHTML

    document.querySelector('#txt-edit-id').value = id
    document.querySelector('#txt-edit-title').value = title
    document.querySelector('#txt-edit-body').value = body

    // Remove the 'disabled' attribute in Update button
    // removeAttribute(<attributeToBeRemove>)
    document.querySelector('#btn-submit-update').removeAttribute('disabled')
}

// Update Post
document.querySelector('#form-edit-post').addEventListener('submit', e => {
    e.preventDefault()

    fetch('https://jsonplaceholder.typicode.com/posts/1', {
        method: 'PUT',
        body: JSON.stringify({
            id: document.querySelector('#txt-edit-id').value,
            title: document.querySelector('#txt-edit-title').value,
            body: document.querySelector('#txt-edit-body').value,
            userId: 1
        }),
        headers: { 'Content-type': 'application/json; charset=UTF-8'}
    })
    .then(response => response.json())
    .then(data => {
        console.log(data)
        alert('Successfully updated.')

        document.querySelector('#txt-edit-id').value = null
        document.querySelector('#txt-edit-title').value = null
        document.querySelector('#txt-edit-body').value = null
        
        // Will put the 'disabled' attribute to Update button
        // setAttribute(<attributeToBeSet>, options)
        document.querySelector('#btn-submit-update').setAttribute('disabled', true)
    })
})

// Mini-Activity:
// Retrieve/Get a single post
// Print the post in the console

let getPost = (id)=> {
    fetch('https://jsonplaceholder.typicode.com/posts/' + id)
    .then((response) => response.json())
    .then((data) => console.log(data))
}

// Delete a post
const deletePost = (id) => {
    console.log(id)
    document.querySelector(`#post-${id}`).remove()
}